#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define ROW 4
#define COL 5

int *matrix;

void *factorial(void *arg) {
    int *num = (int*) arg;
    long long result = 1;

    // hitung faktorial dari num
    for (int i = 1; i <= *num; i++) {
        result *= i;
    }

    pthread_exit((void*) result);
}

int main() {
    // membuat key untuk shared memory
    key_t key = 1234;

    // mendapatkan id dari shared memory
    int shmid = shmget(key, sizeof(int) * ROW * COL, 0666);

    // attach shared memory ke variable matrix
    matrix = (int*) shmat(shmid, NULL, 0);

    // print matriks hasil perkalian
    printf("Matriks hasil perkalian:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%d ", *(matrix + i * COL + j));
        }
        printf("\n");
    }

    // membuat thread untuk menghitung faktorial dari setiap angka pada matriks
    pthread_t tid[ROW * COL];
    int index = 0;

    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            int num = *(matrix + i * COL + j);
            pthread_create(&tid[index], NULL, factorial, (void*) &num);
            index++;
        }
    }

    // join thread dan print hasil faktorial pada matriks
    index = 0;

    printf("\nMatriks hasil faktorial:\n");
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            long long result;
            pthread_join(tid[index], (void**) &result);
            printf("%lld ", result);
            index++;
        }
        printf("\n");
    }

    // detach dan delete shared memory
    shmdt(matrix);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}

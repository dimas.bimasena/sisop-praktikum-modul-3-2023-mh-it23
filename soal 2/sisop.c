#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
    int shmid;
    key_t key = 1234;

    // Mengakses shared memory dari program kalian.c
    shmid = shmget(key, sizeof(int)*20, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    int *matC = (int*) shmat(shmid, NULL, 0);
    if (matC == (int*) -1) {
        perror("shmat");
        exit(1);
    }

    printf("Hasil perkalian matriks:\n");

    // Menampilkan matriks hasil perkalian
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", matC[i*5+j]);
        }
        printf("\n");
    }

    // Menghitung faktorial untuk setiap elemen matriks
    printf("\nHasil faktorial:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            int fact = 1;
            for (int k = 1; k <= matC[i*5+j]; k++) {
                fact *= k;
            }
            printf("%d ", fact);
        }
        printf("\n");
    }

    // Melepaskan shared memory
    shmdt(matC);

    return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define ROW_1 4
#define COL_1 2
#define ROW_2 2
#define COL_2 5

int main() {
  int matriks1[ROW_1][COL_1], matriks2[ROW_2][COL_2], hasil[ROW_1][COL_2];
  int i, j, k;
  
  // Inisialisasi random seed
  srand(time(0));
  
  // Mengisi matriks 1 dengan angka random dari 1-5
  for (i = 0; i < ROW_1; i++) {
    for (j = 0; j < COL_1; j++) {
      matriks1[i][j] = rand() % 5 + 1;
    }
  }
  
  // Mengisi matriks 2 dengan angka random dari 1-4
  for (i = 0; i < ROW_2; i++) {
    for (j = 0; j < COL_2; j++) {
      matriks2[i][j] = rand() % 4 + 1;
    }
  }
  
  // Mengalikan matriks 1 dan 2
  for (i = 0; i < ROW_1; i++) {
    for (j = 0; j < COL_2; j++) {
      hasil[i][j] = 0;
      for (k = 0; k < COL_1; k++) {
        hasil[i][j] += matriks1[i][k] * matriks2[k][j];
      }
    }
  }
  
  // Menampilkan matriks hasil
  printf("Matriks hasil:\n");
  for (i = 0; i < ROW_1; i++) {
    for (j = 0; j < COL_2; j++) {
      printf("%d ", hasil[i][j]);
    }
    printf("\n");
  }
  
  return 0;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/wait.h>

// Node untuk Huffman tree
typedef struct Node {
    char character;
    int frequency;
    struct Node *left;
    struct Node *right;
} Node;

// Deklarasi fungsi
void countFrequency(char *filename, int *frequency);
Node *buildHuffmanTree(int *frequency);
void encode(Node *root, char *code, char character, char **encodingTable);
void compressFile(char *inputFilename, char *outputFilename, char **encodingTable);
void decompressFile(char *inputFilename, char *outputFilename, Node *root);
void printBinary(char *code);

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Usage: %s <input_file> <output_file>\n", argv[0]);
        exit(1);
    }

    // Buat pipe untuk komunikasi antara parent dan child process
    int pipefd[2];
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(1);
    }

    // Fork process
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(1);
    } else if (pid == 0) {
        // Child process

        // Tutup read-end dari pipe
        close(pipefd[0]);

        // Hitung frekuensi kemunculan huruf dari file input
        int frequency[256] = {0};
        countFrequency(argv[1], frequency);

        // Kirim hasil perhitungan frekuensi huruf ke parent process menggunakan pipe
        if (write(pipefd[1], frequency, sizeof(frequency)) == -1) {
            perror("write to pipe");
            exit(1);
        }

        // Tutup write-end dari pipe
        close(pipefd[1]);

        // Exit child process
        exit(0);
    } else {
        // Parent process

        // Tutup write-end dari pipe
        close(pipefd[1]);

        // Baca frekuensi huruf dari child process menggunakan pipe
        int frequency[256];
        if (read(pipefd[0], frequency, sizeof(frequency)) == -1) {
            perror("read from pipe");
            exit(1);
        }

        // Tutup read-end dari pipe
        close(pipefd[0]);

        // Bangun Huffman tree dari frekuensi huruf
        Node *root = buildHuffmanTree(frequency);

        // Buat encoding table
        char *encodingTable[256];
        memset(encodingTable, 0, sizeof(encodingTable));
        char code[256] = "";
        encode(root, code, root->character, encodingTable);

        // Kompres file input
        compressFile(argv[1], argv[2], encodingTable);

        // Dekompres file output
        decompressFile(argv[2], "output.txt", root);

        // Hitung jumlah bit setelah dikompresi dan tampilkan hasilnya
        FILE *fp1 = fopen(argv[1], "rb");
        fseek(fp1, 0, SEEK_END);
        long int fileLength1 = ftell(fp1);
        fclose(fp1);

        FILE *fp2 = fopen(argv[2], "rb");
        fseek(fp2, 0, SEEK_END);
        long int fileLength2 = ftell

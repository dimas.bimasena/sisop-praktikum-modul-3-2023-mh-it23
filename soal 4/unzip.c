#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void downloadFile(){
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", "hehe.zip", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

void unzipFile(){
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "-q", "hehe.zip", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses unzip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }

}

int main(){
    //melakukan download dan zip
    downloadFile();
    unzipFile();

    return 0;
}

# Laporan Resmi Modul 3 Sistem Operasi
Adimasdefatra Bimasena 5027211040

# Soal 1

Program C ini mengimplementasikan pengkodean Huffman, yang merupakan algoritma kompresi data lossless. Program membaca konten file, membangun pohon Huffman, menghasilkan buku kode, menyandikan data, dan menulis data terkompresi ke file keluaran.

Fungsi utama membuat dua pipa untuk berkomunikasi antara proses induk dan anak. Proses induk membaca isi file masukan, menghitung frekuensi setiap karakter, mengirimkan jumlah frekuensi ke proses anak, menunggu proses anak selesai, membaca data terkompresi dari proses anak, dan menulis data terkompresi ke file keluaran.

Proses anak membaca jumlah frekuensi dari proses induk, membangun pohon Huffman, menghasilkan buku kode, membaca file input, mengkodekan data, mengirimkan data terkompresi ke proses induk, dan keluar.

Fungsi-fungsi yang diimplementasikan dalam program ini adalah:

build_huffman_tree: Fungsi ini mengambil larik frekuensi karakter sebagai input dan membangun pohon Huffman menggunakan min heap. Ini mengembalikan akar pohon Huffman.

swap_nodes: Fungsi ini menukar dua node di heap yang digunakan di build_huffman_tree.

heapify_up: Fungsi ini memulihkan properti min heap dengan memindahkan node ke atas heap.

heapify: Fungsi ini mengembalikan properti min heap dengan memindahkan node ke bawah heap.

build_min_heap: Fungsi ini membangun min heap dari array node. Ia menggunakan heapify untuk mengembalikan properti min heap.


#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 

#define NUM_CHAR 256
#define file "file.txt"

typedef struct Node {
    int freq;
    unsigned char data;
    struct Node *left, *right;
} Node;

Node *build_huffman_tree(unsigned int char_freqs[NUM_CHAR]) {
    int heap_size = 0;
    Node *heap[NUM_CHAR];
    for (int i = 0; i < NUM_CHAR; i++) {
        if (char_freqs[i] > 0) {
            Node *node = (Node *) malloc(sizeof(Node));
            node->freq = char_freqs[i];
            node->data = (unsigned char) i;
            node->left = NULL;
            node->right = NULL;
            heap[heap_size] = node;
            heap_size++;
        }
    }
    build_min_heap(heap, heap_size);

    while (heap_size > 1) {
        Node *left_child = heap[0];
        swapNodes(heap, 0, heap_size - 1);
        heap_size--;
        heapify(heap, 0, heap_size);

        Node *right_child = heap[0];
        swapNodes(heap, 0, heap_size - 1);
        heap_size--;
        heapify(heap, 0, heap_size);

        Node *parent = (Node *) malloc(sizeof(Node));
        parent->freq = left_child->freq + right_child->freq;
        parent->data = 0;
        parent->left = left_child;
        parent->right = right_child;

        heap[heap_size] = parent;
        heap_size++;
        heapifyUp(heap, heap_size - 1);
    }

    return heap[0];
}

void swapNodes(Node *heap[], int i, int j);
void heapifyUp(Node *heap[], int i);
void heapify(Node *heap[], int i, int size);
void build_min_heap(Node *heap[], int size);

int main() 
{ 
    int fd1[2]; 
    int fd2[2]; 
    char input_filename[256];
    FILE *input_file;
    unsigned int char_freqs[NUM_CHAR] = {0};

    pid_t p; 

    if (pipe(fd1)==-1) 
    { 
        fprintf(stderr, "Pipe Failed" ); 
        return 1; 
    } 
    if (pipe(fd2)==-1) 
    { 
        fprintf(stderr, "Pipe Failed" ); 
        return 1; 
    } 

    p = fork(); 

    if (p < 0) 
    { 
        fprintf(stderr, "fork Failed" ); 
        return 1; 
    } 

    // Parent process 
    else if (p > 0) 
    { 
        close(fd1[0]); 
        close(fd2[1]);

        // Open input file for reading
        input_file = fopen(file, "r");
        if (!input_file) {
            printf("Failed to open input file.\n");
            return 1;
        }

        // Count frequency of each character in the file
        int c;
        while ((c = fgetc(input_file)) != EOF) {
            char_freqs[c]++;
        }

        // Send the frequency count to child process
        write(fd1[1], char_freqs, sizeof(char_freqs));
        close(fd1[1]);

        wait(NULL); // Wait for child process to finish

        // Read the compressed data from child process
        char compressed_data[NUM_CHAR];
        int compressed_size;
        read(fd2[0], &compressed_size, sizeof(int));
        read(fd2[0], compressed_data, compressed_size);
        close(fd2[0]);

        // Write compressed data to output file
        FILE *output_file = fopen("compressed.bin", "wb");
        if (!output_file) {
            printf("Failed to open output file.\n");
            return 1;
        }
        fwrite(compressed_data, 1, compressed_size, output_file);
        fclose(output_file);

        printf("Compression complete.\n");
    } 

    // child process 
    else
    { 
        close(fd1[1]);
        close(fd2[0]);

        // Read the frequency count from parent process
        unsigned int char_freqs[NUM_CHAR];
        read(fd1[0], char_freqs, sizeof(char_freqs));
        close(fd1[0]);

        // Build the Huffman tree
        // ...

        // Generate the codebook
        // ...

        // Read the input file and encode the data
        FILE *input_file;
        input_file = fopen(file, "r");
        if (!input_file) {
            printf("Failed to open input file.\n");
            exit(1);
        }
        char compressed_data[NUM_CHAR];
        int compressed_size = 0;
        int c;
        while ((c = fgetc(input_file)) != EOF) {
            // Encode the character using the codebook
            // ...
            compressed_size += code_size;
        }
        fclose(input_file);

        // Write the compressed data to the parent process
        write(fd2[1], &compressed_size, sizeof(int));
        write(fd2[1], compressed_data, compressed_size);
        close(fd2[1]);

        exit(0);
    } 
    return 0;
} 

void swap_nodes(Node *heap[], int i, int j) {
    Node *temp = heap[i];
    heap[i] = heap[j];
    heap[j] = temp;
}

void heapify_up(Node *heap[], int i) {
    while (i > 0 && heap[i]->freq < heap[(i - 1) / 2]->freq) {
        swap_nodes(heap, i, (i - 1) / 2);
        i = (i - 1) / 2;
    }
}

void heapify(Node *heap[], int i, int size) {
    int smallest = i;
    int left_child = 2 * i + 1;
    int right_child = 2 * i + 2;

    if (left_child < size && heap[left_child]->freq < heap[smallest]->freq) {
        smallest = left_child;
    }
    if (right_child < size && heap[right_child]->freq < heap[smallest]->freq) {
        smallest = right_child;
    }
    if (smallest != i) {
        swap_nodes(heap, i, smallest);
        heapify(heap, smallest, size);
    }
}

void build_min_heap(Node *heap[], int size) {
    for (int i = size / 2 - 1; i >= 0; i--) {
        heapify(heap, i, size);
    }
}

# Soal 2

Code ini merupakan program dalam bahasa C yang menggunakan shared memory dan thread untuk melakukan perhitungan faktorial dari matriks hasil perkalian yang dihasilkan dari program kalian.c.

Pertama, program ini melakukan import beberapa library yaitu stdio.h, stdlib.h, sys/ipc.h, sys/shm.h, dan pthread.h. Kemudian, program mendefinisikan ukuran shared memory segment dengan SHMSZ sebesar 4 * 5 * sizeof(int), dimana 4 adalah jumlah baris dan 5 adalah jumlah kolom dari matriks hasil perkalian yang akan diolah.

Selanjutnya, program melakukan pengambilan shared memory segment dengan fungsi shmget dan mengattach shared memory segment ke variabel shm dengan fungsi shmat. Program kemudian membuat array threads untuk menyimpan thread-thread yang akan dibuat. Thread-thread tersebut akan menghitung faktorial dari setiap elemen matriks hasil perkalian, yang kemudian akan disimpan pada array threads.

Setelah itu, program menampilkan matriks hasil perkalian yang dihasilkan dari program kalian.c menggunakan looping for. Di dalam looping tersebut, program melakukan penghitungan faktorial dari setiap elemen matriks hasil perkalian menggunakan fungsi pthread_create dan menyimpan hasil faktorial pada array threads.

Setelah semua thread selesai dijalankan, program menampilkan hasil faktorial dari setiap elemen matriks menggunakan looping for dan fungsi pthread_join untuk mendapatkan nilai kembalian dari setiap thread. Hasil faktorial kemudian akan ditampilkan pada layar dengan printf.

Terakhir, program melepaskan shared memory segment dengan fungsi shmdt dan program berakhir dengan mengembalikan nilai 0.


#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#define SHMSZ 4 * 5 * sizeof(int)

// fungsi untuk menghitung faktorial
void *fact(void *arg) {
    int *num = (int *) arg;
    unsigned long long int fact_val = 1;
    for (int i = 1; i <= *num; i++) {
        fact_val *= i;
    }
    unsigned long long int *result = malloc(sizeof(unsigned long long int));
    *result = fact_val;
    return (void*)result;
}

int main() {
    int shmid;
    key_t key = 8080;
    int (*shm)[5];

    // mengambil shared memory segment 
    if ((shmid = shmget(key, SHMSZ, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    // meng-attach shared memory segment
    if ((long long int *)(shm = shmat(shmid, NULL, 0)) == (long long int *) -1) {
        perror("shmat");
        exit(1);
    }
    
    // membuat array thread
    pthread_t threads[4 * 5];
    int thread_count = 0;

    // menampilkan matriks hasil perkalian dari program kalian.c
    printf("Matriks hasil perkalian dari program kalian.c:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", shm[i][j]);
            // hitung faktorial 
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            int *num = &shm[i][j];
            pthread_create(&threads[thread_count++], &attr, fact, num);
        
        }
        printf("\n");
    }
    printf("\n");
    
    //menampilkan hasil faktorial dari matriks 
    printf("Hasil faktorial setiap elemen matriks :\n");
    for (int i = 0; i < thread_count; i++) {
        void *result;
        pthread_join(threads[i], &result);
        unsigned long long int *fact = (unsigned long long int *) result;
        printf("%lld ", *fact);
        free(result);
        if ((i+1) % 5 == 0) {
            printf("\n");
        }
    }
    printf("\n");

    // melepaskan shared memory segment
    shmdt((void *) shm);

    return 0;
}

(cinta)


Program ini melakukan perkalian matriks 4x2 dan 2x5, dan menyimpan hasilnya ke dalam shared memory. Program menggunakan beberapa library standar seperti stdio.h, stdlib.h, time.h, string.h, sys/ipc.h, dan sys/shm.h.

Pertama-tama, program menentukan key untuk shared memory, yaitu 8080, dan mendefinisikan ukuran matriks pertama dan kedua. Selanjutnya, program mengisi matriks pertama dan kedua dengan angka random antara 1-5 dan 1-4 menggunakan fungsi rand().

Program kemudian melakukan perkalian matriks dengan cara melakukan perulangan tiga kali, di mana perulangan pertama dan kedua digunakan untuk mengakses baris dan kolom masing-masing matriks, dan perulangan ketiga digunakan untuk menghitung hasil perkalian matriks. Hasil perkalian matriks kemudian disimpan dalam matriks result.

Setelah hasil perkalian matriks ditampilkan ke layar, program kemudian membuat shared memory dengan ukuran 4x5 integer dengan menggunakan fungsi shmget(). Jika berhasil, program meng-attach shared memory ke variabel sharedMem menggunakan fungsi shmat() dan meng-copy hasil perkalian matriks dari matriks result ke shared memory menggunakan fungsi memcpy().

Program ini menggunakan typedef untuk mendefinisikan tipe data int_array sebagai array integer dengan panjang 5. Ukuran matriks pertama dan kedua didefinisikan sebagai array 2 dimensi dengan tipe data int. Variabel i, j, dan k digunakan sebagai indeks untuk perulangan. Program menggunakan fungsi printf() untuk menampilkan hasil perkalian matriks ke layar dan fungsi perror() untuk menampilkan pesan kesalahan jika terjadi kesalahan dalam memanggil fungsi shmget() atau shmat().
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>

int main()
{
    key_t key = 8080;
    int sharedMem_id;
    typedef int int_array[5];
    int_array *sharedMem;
    
    // Ukuran matriks pertama adalah 4x2 dan matriks kedua 2x5
    int mat1[4][2], mat2[2][5], result[4][5];
    int i, j, k;

    // Inisialisasi seed random
    srand(time(NULL));

    // Mengisi matriks pertama dengan angka random antara 1-5
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 2; j++) {
            mat1[i][j] = rand() % 5 + 1;
        }
    }

    // Mengisi matriks kedua dengan angka random antara 1-4
    for (i = 0; i < 2; i++) {
        for (j = 0; j < 5; j++) {
            mat2[i][j] = rand() % 4 + 1;
        }
    }

    // Melakukan perkalian matriks
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            result[i][j] = 0;
            for (k = 0; k < 2; k++) {
                result[i][j] += mat1[i][k] * mat2[k][j];
            }
        }
    }

    // Menampilkan matriks hasil perkalian ke layar
    printf("Matriks hasil perkalian:\n");
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 5; j++) {
            printf("%d ", result[i][j]);
        }
        printf("\n");
    }

    if ((sharedMem_id = shmget(key, 4 * 5 * sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }


    if ((void*)(sharedMem = shmat(sharedMem_id, NULL, 0)) == (void *) -1) {
        perror("shmat");
        exit(1);
    }

    memcpy(sharedMem, result, 4 * 5 * sizeof(int));
   return 0;
}

(Kalian)

Code tersebut merupakan program C yang menggunakan shared memory untuk melakukan operasi faktorial pada setiap elemen dari matriks yang dihasilkan dari perkalian matriks sebelumnya


Pada bagian ini, diatur library yang diperlukan dan definisi konstanta SHMSZ yang merupakan ukuran dari shared memory yang dibutuhkan untuk menyimpan matriks hasil perkalian. Selain itu, terdapat juga sebuah fungsi fact yang menerima satu parameter integer dan mengembalikan hasil faktorial dari parameter tersebut. Fungsi ini nantinya akan digunakan untuk menghitung faktorial setiap elemen matriks.

Kemudian, pada fungsi main(), diatur variabel shmid untuk menyimpan ID dari shared memory segment. Selain itu, variabel key diatur untuk menyimpan kunci unik untuk shared memory segment, dan (*shm)[5] merupakan pointer ke shared memory segment.


Pada bagian ini, pertama-tama dibuat shared memory segment dengan menggunakan shmget(), dengan parameter key sebagai kunci unik, SHMSZ sebagai ukuran shared memory segment, dan IPC_CREAT | 0666 sebagai permission untuk mengakses shared memory segment. Jika pembuatan shared memory segment gagal, maka akan diberikan error message melalui perror() dan program akan berhenti.

Setelah shared memory segment berhasil dibuat, program akan meng-attach shared memory segment tersebut menggunakan shmat(). Jika attach shared memory segment gagal, maka akan diberikan error message melalui perror() dan program akan berhenti.
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHMSZ 4 * 5 * sizeof(int)

// fungsi faktorial
unsigned long long int fact(int arg) {
    unsigned long long int result = 1;
    for (unsigned long long int i = 2; i <= arg; i++) {
        result *= i;
    }
    return result;
}

int main() {
    int shmid;
    key_t key = 8080;
    int (*shm)[5]; // pointer ke shared memory

    // membuat shared memory segment
    if ((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    // meng-attach shared memory segment 
    if ((shm = shmat(shmid, NULL, 0)) == (int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }

    // mengambil data shared memory dan menampilkannya
    printf("Hasil kali matriks:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            printf("%d ", shm[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    // menampilkan hasil faktorial
    printf("Hasil faktorial setiap elemen matriks:\n");
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            unsigned long long int fact_val = fact(shm[i][j]);

            printf("%llu ", fact_val);
        }
        printf("\n");
    }
    printf("\n");

    return 0;
}

(Sisop) 


# Soal 3

Code ini adalah sebuah program dalam bahasa C yang menggunakan sistem message queue untuk menerima pesan dan menjalankan perintah sesuai dengan isi pesan yang diterima.

Pada awal program, terdapat pembuatan file playlist.txt jika belum ada dengan menggunakan fungsi access dan system.

Selanjutnya, program masuk dalam sebuah loop while(1) yang akan berjalan terus menerus selama program dijalankan.

Di dalam loop, program melakukan inisialisasi key dan msgid menggunakan fungsi ftok dan msgget untuk mengambil message queue yang sudah ada atau membuat baru jika belum ada.

Kemudian, program menunggu pesan masuk dengan menggunakan fungsi msgrcv. Setelah pesan diterima, program mengecek isi pesan menggunakan fungsi strcmp. Jika isi pesan adalah "DECRYPT", program akan melakukan dekripsi. Jika isi pesan adalah "LIST", program akan menampilkan isi dari file playlist.txt menggunakan perintah "cat". Jika isi pesan adalah "PLAY", program akan memutar musik. Jika isi pesan adalah "ADD", program akan menambahkan lagu ke 



#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

int main() {
    char *filename = "playlist.txt";
    if(access(filename, F_OK) == -1){
        system("touch playlist.txt");
    }

    while(1){
        key_t key;
        int msgid;
        struct message_buffer buffer;

        key = ftok("user.c", 'A');
        msgid = msgget(key, 0666 | IPC_CREAT);

        msgrcv(msgid, &buffer, sizeof(buffer), 1, 0);
        printf("Received: %s\n", buffer.message_text);

        if(strcmp(buffer.message_text, "DECRYPT") == 0) {
            // decrypt
        }
        else if(strcmp(buffer.message_text, "LIST") == 0) {
            system("cat playlist.txt");
        }
        else if(strcmp(buffer.message_text, "PLAY") == 0) {
            // play
        }
        else if(strcmp(buffer.message_text, "ADD") == 0) {
            // add
        }
        else {
            printf("UNKNOWN COMMAND\n");
        }

        msgctl(msgid, IPC_RMID, NULL);


    }
    return 0;
}

(stream) 


Code ini adalah program sederhana yang digunakan untuk mengirim pesan ke sebuah message queue menggunakan sistem V IPC. Program ini terdiri dari beberapa header file, yaitu <stdio.h>, <stdlib.h>, <string.h>, dan <sys/msg.h>.

Program ini memiliki struktur data struct message_buffer yang memiliki dua variabel, yaitu message_type bertipe long dan message_text bertipe char dengan ukuran MAX_SIZE (1024).

Fungsi main pada program ini melakukan beberapa hal:

Membuat sebuah key menggunakan fungsi ftok() dengan argumen user.c dan karakter A.
Mencoba untuk mendapatkan identifier message queue menggunakan fungsi msgget() dengan argumen key dan permission 0666 | IPC_CREAT.
Mengisi variabel buffer.message_type dengan nilai 1.
Meminta input dari user berupa command menggunakan fungsi fgets() dengan argumen stdin sebagai input file pointer, variabel buffer.message_text sebagai variabel yang akan menyimpan input tersebut, dan MAX_SIZE sebagai ukuran maksimum input.
Mengirim pesan ke message queue menggunakan fungsi msgsnd() dengan argumen message queue identifier, alamat dari variabel buffer, ukuran variabel buffer, dan flag 0.

Dalam konteks yang lebih luas, program ini merupakan bagian dari sebuah sistem yang dapat melakukan beberapa command, seperti mengenkripsi file, menambahkan file ke dalam playlist, memutar file di playlist, dan menampilkan daftar file yang ada di playlist. Program ini bertanggung jawab untuk mengirimkan command dari pengguna ke program utama yang menjalankan command-command tersebut.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

int main() {
    key_t key;
    int msgid;
    struct message_buffer buffer;

    key = ftok("user.c", 'A');
    msgid = msgget(key, 0666 | IPC_CREAT);

    buffer.message_type = 1;
    printf("Enter command: \n");
    fgets(buffer.message_text, MAX_SIZE, stdin);

    msgsnd(msgid, &buffer, sizeof(buffer), 0);
    return 0;
}

(user) 




# Soal 4


Program ini adalah sebuah program C yang melakukan proses download dan unzip file menggunakan dua fungsi terpisah, yaitu downloadFile() dan unzipFile().

Pada downloadFile(), program menggunakan fork() untuk membuat proses baru. Jika fork() gagal membuat proses baru, maka program akan keluar dengan mengeluarkan pesan error. Jika fork() berhasil membuat proses baru, maka program anak akan menjalankan perintah execl() untuk menjalankan perintah wget pada direktori /usr/bin/wget. Perintah wget akan mendownload file dari alamat URL yang ditentukan dengan argumen -O yang mengatur nama file yang didownload menjadi hehe.zip dan argumen -q untuk menonaktifkan output yang dihasilkan. Jika perintah wget gagal, program anak akan keluar dengan mengeluarkan pesan error.

Pada unzipFile(), program menggunakan fork() untuk membuat proses baru. Jika fork() gagal membuat proses baru, maka program akan keluar dengan mengeluarkan pesan error. Jika fork() berhasil membuat proses baru, maka program anak akan menjalankan perintah execl() untuk menjalankan perintah unzip pada direktori /usr/bin/unzip. Perintah unzip akan meng-unzip file hehe.zip ke direktori kerja saat ini dan argumen -q digunakan untuk menonaktifkan output yang dihasilkan. Jika perintah unzip gagal, program anak akan keluar dengan mengeluarkan pesan error.

Setelah proses download dan unzip selesai, program utama akan keluar dengan mengembalikan nilai 0.




#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

void downloadFile(){
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/wget", "wget", "-q", "-O", "hehe.zip", "https://drive.google.com/uc?export=download&id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

void unzipFile(){
    pid_t pid = fork();

    if (pid == -1) {
        exit(EXIT_FAILURE);
    }

    if (pid == 0) {
        execl("/usr/bin/unzip", "unzip", "-q", "hehe.zip", NULL);
        exit(EXIT_FAILURE);
    }

    int status;
    waitpid(pid, &status, 0);

    if (!WIFEXITED(status)) {
        printf("Proses unzip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }

}

int main(){
    //melakukan download dan zip
    downloadFile();
    unzipFile();

    return 0;
}

(unzip)

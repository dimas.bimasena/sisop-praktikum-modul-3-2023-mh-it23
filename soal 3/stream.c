#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>
#include <unistd.h>
#include <ctype.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

// function to sort playlist.txt alphabetically
void sort_playlist() {
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int lines = 0;

    fp = fopen("playlist.txt", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    // count number of lines in playlist.txt
    while ((read = getline(&line, &len, fp)) != -1) {
        lines++;
    }
    rewind(fp);

    // read playlist.txt into an array
    char **playlist = malloc(lines * sizeof(char *));
    for (int i = 0; i < lines; i++) {
        playlist[i] = malloc(MAX_SIZE * sizeof(char));
        getline(&playlist[i], &len, fp);
    }

    // sort array alphabetically using bubble sort
    for (int i = 0; i < lines - 1; i++) {
        for (int j = 0; j < lines - i - 1; j++) {
            if (strcmp(playlist[j], playlist[j + 1]) > 0) {
                char *temp = playlist[j];
                playlist[j] = playlist[j + 1];
                playlist[j + 1] = temp;
            }
        }
    }

    // write sorted array back into playlist.txt
    fp = fopen("playlist.txt", "w");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    for (int i = 0; i < lines; i++) {
        fprintf(fp, "%s", playlist[i]);
        free(playlist[i]);
    }

    free(playlist);
    fclose(fp);
}

// function to decrypt song-playlist.json to playlist.txt
void decrypt() {
    FILE *fpin = fopen("song-playlist.json", "r");
    FILE *fpout = fopen("playlist.txt", "w");
    char ch;

    if (fpin == NULL || fpout == NULL) {
        printf("Error opening files\n");
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(fpin)) != EOF) {
        // perform decryption operation
        if (isalpha(ch)) {
            ch = toupper(ch);
            ch = 'Z' - (ch - 'A');
        }
        fputc(ch, fpout);
    }

    fclose(fpin);
    fclose(fpout);

    sort_playlist();
}

int main() {
    char *filename = "playlist.txt";
    if(access(filename, F_OK) == -1){
        system("touch playlist.txt");
    }

    while(1){
        key_t key;
        int msgid;
        struct message_buffer buffer;

        key = ftok("user.c", 'A');
        msgid = msgget(key, 0666 | IPC_CREAT);

        msgrcv(msgid, &buffer, sizeof(buffer), 1, 0);
        printf("Received: %s\n", buffer.message_text);

        if(strcmp(buffer.message_text, "DECRYPT") == 0) {
            decrypt();
            printf("Decrypted and sorted playlist.txt\n");
        }
        else if(strcmp(buffer.message_text, "LIST") == 0) {
            system("

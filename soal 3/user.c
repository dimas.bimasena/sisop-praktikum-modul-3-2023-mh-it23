#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/msg.h>

#define MAX_SIZE 1024

struct message_buffer {
    long message_type;
    char message_text[MAX_SIZE];
};

int main() {
    // mendeklarasi variable yg akan digunakan
    key_t key;
    int msgid;
    struct message_buffer buffer;

    while (1){
        // membuat key dan msgid
        key = ftok("user.c", 'A');
        msgid = msgget(key, 0666 | IPC_CREAT);

        // menginput command
        buffer.message_type = 1;
        printf("Enter command: \n");
        fgets(buffer.message_text, MAX_SIZE, stdin);

        // mengirim ke stream.c
        msgsnd(msgid, &buffer, sizeof(buffer), 0);
    }
    
    return 0;
}
